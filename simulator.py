import os
import time
import signal
import random
import threading
import subprocess
import traceback
import animation_framework
import salabim as sim
from serverphy import serverphy_t
from server import server_thread_fn, g_nodes_dict
from interface import *
from radiomedium import g_radio_medium
from animation_framework import g_simulation_ground

# Return x,y,z (positive) list using cuboid_limits
def get_random_location(limits):
    x_min = 0
    x_max = limits[0]

    y_min = 0
    y_max = limits[1]

    z_min = 0
    z_max = limits[2]

    x = x_min + (random.random()-0.5)*(x_max - x_min)
    y = y_min + (random.random()-0.5)*(y_max - y_min)
    z = z_min + (random.random()-0.5)*(z_max - z_min)
    return (x, y, z)

class time_tracker_t(sim.Component):
    def __init__(self, duration):
        super(time_tracker_t, self).__init__()
        self.duration = duration

    def process(self):
        interval = 1
        while True:
            yield self.hold(interval)
            now = self.env.now()
            print("\rSimulator : {0:6.2f} % of simulation, {1} of total {2}".format(100*now/self.duration, now, self.duration), end='')        

def signal_handler(sig, frame):
        print('\nYou pressed Ctrl+C!')
        os._exit(0)

if __name__=="__main__":
    signal.signal(signal.SIGINT, signal_handler)
    env = sim.Environment(trace=False)

    simulation_speed = 5
    animation_framework.start_animation(simulation_speed)

    duration = 60*60
    cuboid_limits = (4*1000, 2*1000, 200)

    time_tracker = time_tracker_t(duration)

    g_radio_medium.init(700*1E6, range(intf.RADIO_CHANNEL_MAX_COUNT), intf.RADIO_FREQ_868MHz, 3)

    server_thread = threading.Thread(target=server_thread_fn, args=())
    server_thread.start()

    num_nodes = 50
    node_application = "./example_application.elf"

    # Yield main-thread for serverthread to execute and accept connections
    print("Simulator : Waiting for {0} nodes to get registered into the simulator".format(num_nodes))
    time.sleep(2)


    # start individual node applications
    node_processes = []
    log_files = []
    for node_id in range(num_nodes):
        log_file = open("./op_files/op_log_node_{0}.txt".format(node_id), "w", 10)
        process = subprocess.Popen([node_application, "-n", str(node_id)], stdout=log_file)
        node_processes.append(process)

    # Wait for all node-phys to register with simulator
    while True:
        if (serverphy_t.num_phys_created==num_nodes):
            break
        else:
            time.sleep(0.2)

    print("Simulator : All nodes registered into simulator")
    time.sleep(1)

    node_time_offset = lambda : random.random()*30

    for node_id, node_inst in g_nodes_dict.items():
        phase = random.random()*num_nodes
        (x, y, z) = get_random_location(cuboid_limits)
        if node_id==0:
            x = y = z = 0
        g_simulation_ground.create_new_node(x, y, 5000, 2)
        node_inst.set_server_time_offset(node_time_offset())
        node_phy = node_inst.get_server_phy()
        node_phy.set_location(x, y, z)
        node_phy.set_drift_rate(random.random()/1000)
        print("Setting location as {0}".format([x,y,z]))
            

    print("Simulator : Starting discrete-event-simulation now")
    env.animate = True;
    env.animation_parameters(width=1, height=1, speed=simulation_speed)
    env.run(duration)

    print("Simulator : Simulation completed now duration = {0}".format(duration))

    # kill indivial node applications since the simulation is over
    for process in node_processes:
        process.terminate()

    for log_file in log_files:
        log_file.close()
