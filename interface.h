#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#define SERVER_PORT 3333
#define MAX_MESSAGE_SIZE    1500
#define MAX_EVENT_MESSAGE_SIZE  100
#define MAX_WAIT_STATES     10

#include <mutex>

using namespace std;

typedef enum {
    RADIO_FREQ_433MHz,
    RADIO_FREQ_868MHz,
    RADIO_FREQ_915MHz,
    RADIO_FREQ_2400MHz,
} radio_freq_enum_t;

typedef enum {
    DATA_RATE_SF_12 = 300,
    DATA_RATE_SF_11 = 550,
    DATA_RATE_SF_10 = 1000,
    DATA_RATE_SF_09 = 1800,
    DATA_RATE_SF_08 = 3200,
    DATA_RATE_SF_07 = 5600,
    DATA_RATE_SF_06 = 9600,
} data_rate_enum_t;

typedef enum {
    SENSITIVITY_SF_12 = -137,
    SENSITIVITY_SF_11 = -134,
    SENSITIVITY_SF_10 = -132,
    SENSITIVITY_SF_09 = -129,
    SENSITIVITY_SF_08 = -126,
    SENSITIVITY_SF_07 = -123,
    SENSITIVITY_SF_06 = -118,
} sensitivity_level_enum_t;

typedef enum {
    RADIO_CHANNEL_0,
    RADIO_CHANNEL_1,
    RADIO_CHANNEL_2,
    RADIO_CHANNEL_3,
    RADIO_CHANNEL_MAX_COUNT,
} radio_channel_enum_t;

typedef enum {
    MESSENGER_INVALID_ENUM,

    MESSENGER_PHY_REGISTER_CMD,     /* <1. msg_len> <2. message_enum_t> <3. messenger_register_struct_t> */
    MESSENGER_THREAD_REGISTER_CMD,  /* <1. msg_len> <2. message_enum_t> <3. messenger_register_struct_t> */
    MESSENGER_TIMER_REGISTER_CMD,   /* <1. msg_len> <2. message_enum_t> <3. messenger_register_struct_t> */

    MESSENGER_PHY_REGISTER_RSP,
    MESSENGER_THREAD_REGISTER_RSP,
    MESSENGER_TIMER_REGISTER_RSP,

    MESSENGER_REQ_SLEEP_CMD,              /* <1. msg_len> <2. message_enum_t> <3. messenger_sleep_cmd_struct_t> */
    MESSENGER_RSP_SLEEP_CMD,              /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_TX_PACKET_CMD,          /* <1. msg_len> <2. message_enum_t> <3. radio_tx_packet_t> */
    MESSENGER_RSP_TX_PACKET_CMD,          /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_RX_EN_CMD,              /* <1. msg_len> <2. message_enum_t> <3. radio_rx_enable_struct_t> */
    MESSENGER_RSP_RX_EN_CMD,              /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_RX_PACKET_CMD,          /* <1. msg_len> <2. message_enum_t> */
    MESSENGER_RSP_RX_PACKET_CMD,          /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> [optional] <4. radio_rx_packet_t> */

    MESSENGER_REQ_RX_DIS_CMD,             /* <1. msg_len> <2. message_enum_t> */
    MESSENGER_RSP_RX_DIS_CMD,             /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_WAIT_FOR_EVENT_CMD,     /* <1. msg_len> <2. message_enum_t> <3. event_id (int)> */
    MESSENGER_RSP_WAIT_FOR_EVENT_CMD,     /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> <4. messenger_event_struct_t> */

    MESSENGER_REQ_ACTIVATE_EVENT_CMD,     /* <1. msg_len> <2. message_enum_t> <3. messenger_event_struct_t> */
    MESSENGER_RSP_ACTIVATE_EVENT_CMD,     /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_START_TIMER_CMD,        /* <1. msg_len> <2. message_enum_t> <3. timer-id> */
    MESSENGER_RSP_START_TIMER_CMD,        /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_STOP_TIMER_CMD,         /* <1. msg_len> <2. message_enum_t> <3. timer-id> */
    MESSENGER_RSP_STOP_TIMER_CMD,         /* <1. msg_len> <2. message_enum_t> <3. messenger_status_t> */

    MESSENGER_REQ_GET_TIME_CMD,           /* <1. msg_len> <2. message_enum_t> */
    MESSENGER_RSP_GET_TIME_CMD,           /* <1. msg_len> <2. message_enum_t> <3. float> */
} message_enum_t;

typedef struct {
    int event_id;
    int buffer_len;
    int buffer[MAX_EVENT_MESSAGE_SIZE];
} messenger_event_struct_t;

typedef struct {
    int node_id;
    int instance_id;
} messenger_register_struct_t;

typedef struct {
    float duration;
} messenger_sleep_cmd_struct_t;

typedef struct {
    bool result;
} messenger_status_t;

typedef struct {
    uint8_t packet[100];
    int packet_len;
    int tx_rate;    /* bits/second */
    float tx_power;
    radio_freq_enum_t freq;
    radio_channel_enum_t channel;
} radio_tx_packet_t;

typedef struct {
    radio_freq_enum_t freq;
    radio_channel_enum_t channel;
} radio_rx_enable_struct_t;

typedef struct {
    char packet[100];
    int packet_len;
    radio_freq_enum_t freq;
    radio_channel_enum_t channel;
    int rx_rate;
    float rx_power;
    float rx_noise;
    float rx_delay;
    bool next;
} radio_rx_packet_t;

radio_rx_packet_t * malloc_radio_rx_packet_copy_tx(radio_tx_packet_t *packet, float rx_power, float rx_snr, float rx_delay);
void free_radio_rx_packet_copy(radio_rx_packet_t *packet);

typedef struct {
    float time;
} messenger_time_t;

/* message format on TCP socket
 * <len in int><message_enum><message> where len = sizeof(message_enum_t) + message struct size
 */

extern void send_message_common(int socket_fd, mutex *lock_mutex, message_enum_t msg_num, char *storage_buffer, int storage_buffer_len, char * buffer, int buffer_len);

extern bool recv_message_common(int socket_fd, mutex *lock_mutex, message_enum_t *msg_num, char *storage_buffer, int storage_buffer_len, char * buffer, int buffer_len);

#define INTF_LEN_FIELD_SIZE             sizeof(int)
#define INTF_MESSAGE_ENUM_FIELD_SIZE    sizeof(message_enum_t)

#ifdef DES_INTERFACE

class serverbase_t {
    protected:
        int node_id;
        int instance_id;
        char message_buffer[MAX_MESSAGE_SIZE];
        int message_len;

    public:
        int get_node_id(void);
        int get_instance_id(void);
        message_enum_t get_recv_message_num(void);
        float get_req_sleep_duration(void);
        void send_rsp_status(message_enum_t msg_num, bool result);
        void send_rsp_current_time(float time);
        void copy_received_message(char *buffer, int buffer_len);
        int copy_sending_message(char *buffer, int buffer_len);
        void send_message(message_enum_t msg_num, char *buffer, int buffer_len);
};

class serverthread_t : public serverbase_t{
    public:
        /* initializes and sends back registration response */
        serverthread_t(int node_id, int thread_id);
        int get_thread_id(void);
        int get_req_timer_id(void);
        radio_tx_packet_t * get_tx_packet(char *buffer, int buffer_len);
        int get_event_id(void);
        messenger_event_struct_t * get_messenger_event_struct(char *buffer, int buffer_len);
        void send_messenger_event_struct(bool status, messenger_event_struct_t *event_struct);
        radio_rx_enable_struct_t * get_rx_enable_struct(void);
        void send_rx_packet_rsp(bool result, radio_rx_packet_t *packet);
};

class servertimer_t : public serverbase_t {
    public:
        /* initializes and sends back registration response */
        servertimer_t(int node_id, int timer_id);
        int get_timer_id(void);
};

class serverphy_t : public serverbase_t {
    public:
        serverphy_t(int node_id, int phy_id);
};

class server_t {
    private:
        char message_buffer[MAX_MESSAGE_SIZE];
        int message_len;

    public:
        void copy_received_message(char *buffer, int buffer_len);
        int copy_sending_message(char *buffer, int buffer_len);
        message_enum_t get_request_information(int *node_id, int *instance_id);
};

#else

/* Forward declaration */

class nodethread_base_t {
    protected:
        char storage_buffer[MAX_MESSAGE_SIZE];
        int socket_fd;
        mutex mlock;
        int node_id;
        int instance_id;

        void send_message(message_enum_t msg_num, char * buffer, int buffer_len);
        bool recv_message(message_enum_t *msg_num, char * buffer, int buffer_len);

    protected:
        void register_with_server(message_enum_t msg_num, message_enum_t expected_msg_num, int node_id, int instance_id);

    public:
        nodethread_base_t(void);
        void sleep(float duration);
        float get_time(void);
};

class nodethread_t; class nodetimer_t; class nodephy_t;

typedef void (*nodethread_fptr_t)(nodethread_t *);

class nodethread_t : public nodethread_base_t {
    friend class nodetimer_t;
    friend class nodephy_t;
    protected:
        thread *thread_handle;
        bool is_initialized;
        bool should_stop;

    protected:
        bool is_mainthread;
        nodethread_fptr_t func_ptr;
        void process(void);

    private:
        /* Used by friend class nodetimer_t */
        void start_timer_from_thread(int timer_id);
        void stop_timer_from_thread(int timer_id);

        /* Used by friend class nodephy_t */
        bool tx_packet_from_thread(radio_tx_packet_t *packet);
        bool rx_enable_from_thread(radio_rx_enable_struct_t *enable_struct);
        bool rx_packet_from_thread(radio_rx_packet_t *rx_packet);
        bool rx_disable_from_thread(void);

    public:
        nodethread_t(int node_id, int thread_id, nodethread_fptr_t fptr);
        int get_node_id(void);
        int get_thread_id(void);
        void start(void);
        void stop(void);
        void wait_for_event(int event_id, messenger_event_struct_t *event_struct);
        void activate_event(messenger_event_struct_t *event_struct);
};

typedef void (*nodetimer_callback_fptr_t)(nodetimer_t *);

class nodetimer_t : public nodethread_base_t {
    protected:
        thread *thread_handle;
        bool is_initialized;
        bool should_stop;
        float period;
        float offset;
        bool offset_enabled;
        nodetimer_callback_fptr_t callback_handler;

    protected:
        void process(void);

    public:
        nodetimer_t(int node_id, int timer_id, float period, nodetimer_callback_fptr_t fptr);
        int get_node_id(void);
        int get_timer_id(void);
        void add_offset(float delay);
        void start_timer_from_thread(nodethread_t *thread);
        void stop_timer_from_thread(nodethread_t *thread);
        void stop_timer_from_timer_callback(void);
        void update_callback_function(nodetimer_callback_fptr_t fptr);
};

class nodephy_t : public nodethread_base_t {
    static int num_instances;

    private:
        bool is_initialized;
        bool is_receiving;

    public:
        nodephy_t(int node_id);
        bool tx_packet_from_thread(radio_tx_packet_t *packet, nodethread_t *thread);
        bool rx_enable_from_thread(radio_rx_enable_struct_t *enable_struct, nodethread_t *thread);
        bool rx_packet_from_thread(radio_rx_packet_t *rx_packet, nodethread_t *thread);
        bool rx_disable_from_thread(nodethread_t *thread);
};

#endif /* for DES_INTERFACE */

#endif /* for __INTERFACE_H__ */
