import random
from math import floor
from threading import Lock
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
from time import sleep
import code
from threading import Thread

async_mode = None

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
socketio = SocketIO(app, async_mode=async_mode)
background_message_thread = None
interpreter_thread = None
socketio_thread = None
thread_lock = Lock()

class simulation_ground_t:
    _total_instances = 0
    def __init__(self):
        if simulation_ground_t._total_instances==0:
            simulation_ground_t._total_instances += 1
        else:
            assert 0
        
        self.isConnected = False
        self.js_messages_list = [];
        self.node_create_messages = []; # Store node-create messages to transfer if socketio disconnects and reconnects
        print("simulation_ground_t : Animation web-ui started at http://127.0.0.1:5000")
    
    def init(self, simulation_speed):
        self.simulation_speed = simulation_speed
        message = {
                "id": 4,
                "simulation_speed": simulation_speed,}
        assert len(self.node_create_messages)==0
        self.node_create_messages.append(message)
        self.js_messages_list.append(message)

    def create_new_node(self, location_x, location_y, tx_range_m, tx_duration_s):
        message = { 
                "id": 0, 
                "location_x": location_x, 
                "location_y": location_y, 
                "tx_range_m": tx_range_m, 
                "tx_duration_s": tx_duration_s,}
        self.node_create_messages.append(message)
        self.js_messages_list.append(message)

    def trigger_tx_beacon_message(self, node_idx):
        message = { "id": 1, "node_idx": node_idx, }
        self.js_messages_list.append(message)

    def trigger_tx_data_message(self, node_idx):
        message = { "id": 2, "node_idx": node_idx, }
        self.js_messages_list.append(message)

    def trigger_rx_message(self, node_idx):
        message = { "id": 3, "node_idx": node_idx, }
        self.js_messages_list.append(message)

    def socket_io_connected(self):
        self.js_messages_list = list(self.node_create_messages)
        self.isConnected = True
        #print("simulation_ground_t: socket_io_connected called")

    def socket_io_disconnected(self):
        self.isConnected = False
        self.js_messages_list.clear()
        #print("simulation_ground_t: socket_io_disconnected called")

g_simulation_ground = simulation_ground_t()

def background_message_thread_method():
    while True:
        socketio.sleep(0.001)
        if g_simulation_ground.isConnected:
            if len(g_simulation_ground.js_messages_list):
                message = g_simulation_ground.js_messages_list.pop(0)
                with app.test_request_context('/'):
                    socketio.emit('chat message', message, namespace="/chat")


#def interpreter_thread_method():
#    num_nodes = 20
#    for i in range(num_nodes):
#        x = (random.random()-0.5)*100*1000;
#        y = (random.random()-0.5)*50*1000;
#        g_simulation_ground.create_new_node(x, y, 10000, 2)
#        sleep(0.1)
#    while True:
#        number = floor(random.random()*num_nodes)
#        g_simulation_ground.trigger_tx_beacon_message(number)
#        sleep(0.02)

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@socketio.on('connect', namespace='/chat')
def test_connect():
    global background_message_thread
    #global interpreter_thread
    with thread_lock:
        if background_message_thread is None:
            background_message_thread = socketio.start_background_task(target=background_message_thread_method)
        #if interpreter_thread is None:
        #    interpreter_thread = Thread(target = interpreter_thread_method)
        #    interpreter_thread.start()
    g_simulation_ground.socket_io_connected()

@socketio.on('chat message', namespace='/chat')
def received_chat_message(data):
    #print("Received chat message {0}".format(data))
    emit('chat reply', {'data': 'Something', 'count': 0})

@socketio.on('disconnect', namespace='/chat')
def test_disconnect():
    g_simulation_ground.socket_io_disconnected()

def socketio_thread_method():
    socketio.run(app, debug=False)

def start_animation(simulation_speed):
    g_simulation_ground.init(simulation_speed)
    socketio_thread = Thread(target=socketio_thread_method)
    socketio_thread.start()
