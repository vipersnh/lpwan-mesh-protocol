"use strict";

var g_animation_context = {
    "initialized": false,
    "simulation_speed": 1,
    "nodes_list": [],
    "m2px_scaling": 0.5,
    "node_circle_duration": 3000,
    "duration_scaling": 8000,
};

function create_new_node(location_x, location_y, tx_range_m, tx_duration_s) {
    tx_duration_s = tx_duration_s*g_animation_context.duration_scaling;
    var pixel_x = Math.floor(location_x*g_animation_context.m2px_scaling);
    var pixel_y = Math.floor(location_y*g_animation_context.m2px_scaling);
    var tx_circle_max = Math.floor(tx_range_m*g_animation_context.m2px_scaling)/10;
    var simulation_speed = g_animation_context.simulation_speed;
    var node_circle_rad_min = 5;
    var node_circle_rad_max = 10;
    var node = {
        "location_x": location_x,
        "location_y": location_y,
        "tx_range_m": tx_range_m,
        "tx_duration_s": tx_duration_s,
        "mjs_node_circle": new mojs.Shape({
            x: pixel_x,
            y: pixel_y,
            radius: {[node_circle_rad_min]:node_circle_rad_max},
            fill: "white",
            duration: g_animation_context.node_circle_duration,
        }).then({
            radius: {[node_circle_rad_max]:node_circle_rad_min},
            onComplete: function () { this.replay(); },
            duration: g_animation_context.node_circle_duration,
        }).play(),
        "mjs_tx_data_circle": new mojs.Shape({
            x: pixel_x,
            y: pixel_y,
            radius: {[node_circle_rad_min]: tx_circle_max},
            stroke: "#FD151B",
            fill: "none",
            duration: tx_duration_s/simulation_speed,
            opacity: {1:0},
        }),
        "mjs_tx_beacon_circle": new mojs.Shape({
            x: pixel_x,
            y: pixel_y,
            radius: {[node_circle_rad_min]: tx_circle_max},
            stroke: "#AAF683",
            fill: "none",
            duration: tx_duration_s/simulation_speed,
            opacity: {1:0},
        }),
        "mjs_rx_circle": new mojs.Shape({
            x: pixel_x,
            y: pixel_y,
            radius: {[tx_circle_max/3]:node_circle_rad_min},
            fill: "none",
            stroke: "white",
            opacity: {0:0.5},
            duration: tx_duration_s/simulation_speed,
        }),
    };
    g_animation_context.nodes_list.push(node);
    console.log("Created new node");
    return node;
}

function delete_node(node) {
    var objects = [node.mjs_node_circle, node.mjs_tx_data_circle, node.mjs_tx_beacon_circle, node.mjs_rx_circle];
    var idx;
    var el;
    for (idx=0; idx<objects.length; idx++) {
        el = objects[idx].el;
        el.parentNode.removeChild(el);
    }
}

function trigger_tx_beacon_message(node_idx) {
    if (node_idx < g_animation_context.nodes_list.length) {
        g_animation_context.nodes_list[node_idx].mjs_tx_beacon_circle.play();
    }
}

function trigger_tx_data_message(node_idx) {
    if (node_idx < g_animation_context.nodes_list.length) {
        g_animation_context.nodes_list[node_idx].mjs_tx_data_circle.play();
    }
}

function trigger_rx_message(node_idx) {
    if (node_idx < g_animation_context.nodes_list.length) {
        g_animation_context.nodes_list[node_idx].mjs_rx_circle.play();
    }
}

function do_process(message) {
    switch (message.id) {
        case 0:
            if (g_animation_context.initialized) {
                create_new_node(message.location_x, message.location_y, message.tx_range_m, message.tx_duration_s);
            }
            break;
        case 1:
            if (g_animation_context.initialized) {
                trigger_tx_beacon_message(message.node_idx);
            }
            break;
        case 2:
            if (g_animation_context.initialized) {
                trigger_tx_data_message(message.node_idx);
            }
            break;
        case 3:
            if (g_animation_context.initialized) {
                trigger_rx_message(message.node_idx);
            }
            break;
        case 4:
            /* Initialize simulation parameters */
            console.log(message);
            g_animation_context.simulation_speed = message.simulation_speed
            g_animation_context.initialized = true;
            break;
    }
}

const socket = io.connect('http://' + document.domain + ':' + location.port + '/chat');

socket.on('connect', function() {
});

socket.on('disconnect', function() {
    console.log('<br>Disconnected');
    var idx;
    var node;
    for (idx=0; idx<g_animation_context.nodes_list.length; idx++) {
        node = g_animation_context.nodes_list[idx];
        delete_node(node);
    }
    g_animation_context.nodes_list.length = 0;
    g_animation_context.initialized = false;
    g_animation_context.simulation_speed = 1;
});

socket.on('chat message', function(message) {
    do_process(message);
});

