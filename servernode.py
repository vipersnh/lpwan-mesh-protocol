import salabim as sim

class servernode_t:
    def __init__(self, node_id):
        self.server_node_threads = {}
        self.server_node_timers = {}
        self.server_node_queues = []
        self.server_node_queue_waits = []
        self.server_node_phy = None
        self.server_node_waits = {}
        self.server_node_id = node_id
        self.time_offset = None
        print("servernode->init called {0}".format(node_id))

    def set_server_time_offset(self, offset):
        self.time_offset = offset

    def get_server_time_offset(self):
        return self.time_offset

    def set_server_phy(self, serverphy):
        assert self.server_node_phy==None
        self.server_node_phy = serverphy

    def get_server_phy(self):
        assert self.server_node_phy != None
        return self.server_node_phy

    def get_node_thread_by_id(self, id_val):
        return self.server_node_threads[id_val]

    def get_node_timer_by_id(self, id_val):
        return self.server_node_timers[id_val]

    def add_node_thread(self, server_thread, id_val):
        assert id_val not in self.server_node_threads.keys()
        self.server_node_threads[id_val] = server_thread

    def add_node_timer(self, server_timer, id_val):
        assert id_val not in self.server_node_timers.keys()
        self.server_node_timers[id_val] = server_timer

    def set_event_wait_state(self, event_id, cpp_event_struct):
        if event_id in self.server_node_waits.keys():
            (state, _) = self.server_node_waits[event_id]
        else:
            state = sim.State('wait-events')
        self.server_node_waits[event_id] = (state, cpp_event_struct)
        state.set()

    def get_event_wait_state(self, event_id):
        if event_id in self.server_node_waits.keys():
            (state, _) = self.server_node_waits[event_id]
        else:
            state = sim.State('wait-events')
            self.server_node_waits[event_id] = (state, None)
        return state

    def get_event_wait_struct(self, event_id):
        assert event_id in self.server_node_waits.keys()
        (state, cpp_event_struct) = self.server_node_waits[event_id]
        return cpp_event_struct


