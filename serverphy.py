import copy
import salabim as sim
from radiomedium import *
import interface_wrapper as intf
from interface import recv_message, send_message
from animation_framework import g_simulation_ground

# TODO: Add half-duplex mode by blocking/failing transmission or reception when either is on

class phy_packet_t:
    def __init__(self, cpp_packet, tx_power, tx_channel, radiophy, duration):
        assert type(cpp_packet)==intf.radio_tx_packet_t
        self.cpp_packet = cpp_packet
        self.tx_power = tx_power
        self.tx_channel = tx_channel
        self.radiophy = radiophy
        self.duration = duration
        self.t_start = None
        self.t_end = None

class phy_location_t:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

class serverphy_t(sim.Component):
    num_phys_created = 0
    def __init__(self, sock, node_id, instance_id, node):
        super(serverphy_t, self).__init__()
        self.messenger_sock = sock
        self.node_id = node_id
        self.phy_id = instance_id
        self.servernode = node
        self.phy_location = None
        self.param_location_x = None
        self.param_location_y = None
        self.param_location_z = None
        self.param_drift_rate = None
        self.param_tx_rate = None        
        self.radiomedium = g_radio_medium
        self.tx_enabled = False
        self.rx_enabled = False
        self.received_packets = list()
        self.tx_channel = None
        self.rx_channel = None     
        self.tx_packet = None
        self.cpp_node_phy = intf.serverphy_t(node_id, instance_id)
        self.message_buffer = bytearray(intf.MAX_MESSAGE_SIZE)
        #print("serverphy->__init__ : Created with node_id {0}, phy-id {1} at {2}".format(self.node_id, self.phy_id, self.env.now()))
        self.acknowledge_registration(True)
        serverphy_t.num_phys_created += 1

    def set_location (self, x, y, z):
        # This must be set by the simulator since node's themselves don't know where they are
        self.param_location_x = x
        self.param_location_y = y
        self.param_location_z = z
        self.phy_location = phy_location_t(x, y, z)
 
    def set_drift_rate (self, drift_rate):
        # This must be set by the simulator since node's themselves don't know their drift rate
        self.param_drift_rate = drift_rate

    def send_message(self):
        message_len = self.cpp_node_phy.copy_sending_message(self.message_buffer)
        send_message(self.messenger_sock, self.message_buffer, message_len)

    def acknowledge_registration(self, status):
        self.cpp_node_phy.send_rsp_status(intf.MESSENGER_PHY_REGISTER_RSP, status)
        self.send_message()

    def handle_packet_transmission(self, cpp_packet):
        assert self.tx_packet==None
        assert self.tx_enabled==False
        self.tx_enabled = True
        duration = cpp_packet.packet_len/cpp_packet.tx_rate
        self.tx_channel = self.radiomedium.get_radio_channel(cpp_packet.channel)
        packet = phy_packet_t(cpp_packet, cpp_packet.tx_power, self.tx_channel, self, duration)
        packet.t_start = self.env.now()
        self.tx_channel.speak(packet)
        self.tx_packet = packet
        g_simulation_ground.trigger_tx_data_message(self.node_id)
        return duration, True        

    def handle_packet_transmission_end(self):
        assert self.tx_packet != None
        assert self.tx_enabled==True
        self.tx_packet.t_end = self.env.now()
        self.tx_channel.unspeak(self.tx_packet)
        self.tx_enabled = False
        self.tx_channel = None
        self.tx_packet = None
        yield self.hold(0)

    def enable_packet_reception(self, freq, rx_channel):
        if self.rx_enabled==False:
            assert self.rx_channel==None
            self.rx_channel = self.radiomedium.get_radio_channel(rx_channel)
            self.rx_channel.listen(self)
            self.rx_enabled = True
            self.rx_enable_t_start = self.env.now()
            return True
        else:
            return False

    def disable_packet_reception(self):
        if self.rx_enabled==True:
            self.rx_channel.unlisten(self)
            self.rx_channel = None
            self.rx_enabled = False
            self.rx_enable_t_start = None
            return True
        else:
            return False

    def get_next_received_packet(self):
        if len(self.received_packets):
            return self.received_packets.pop(-1)
        else:
            return None

    def handle_received_packet(self, rx_packet, rx_channel, rx_power, noise_power):
        # Create a new copy of the cpp_packet since its derived in C++ context and needs to keep a copy
        if self.tx_enabled:
            print("serverphy[{0}]->handle_received_packet : Dropping received packet since I'm transmitting now {1}"
                    .format(self.node_id, self.env.now()))
            return
        rx_packet = copy.copy(rx_packet)
        rx_packet.cpp_packet = intf.malloc_radio_rx_packet_copy_tx(rx_packet.cpp_packet, rx_power, noise_power, self.env.now() - self.rx_enable_t_start)
        self.received_packets.append(rx_packet)
        #print("nodephy[{0}]->handle_received_packet : Adding new packet at {1}".format(self.node_id, self.env.now()))

    def set_location (self, x, y, z):
        # This must be set by the simulator since node's themselves don't know where they are
        self.param_location_x = x
        self.param_location_y = y
        self.param_location_z = z
        self.phy_location = phy_location_t(x, y, z)
 
