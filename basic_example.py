# Example - basic.py
import salabim as sim
import time
from threading import Thread


class Car(sim.Component):
    count = 1
    def __init__(self):
        super(Car, self).__init__()
        self.instance_id = Car.count
        Car.count += 1
        if self.instance_id==1:
            self.instances = []

    def process(self):
        while True:
            yield self.hold(1)
            print ("Hello from {0} at {1}".format(self.instance_id, self.env.now()))
            if self.instance_id==1:
                if len(self.instances) < 5:
                    self.instances.append(Car())

objects = []

def myfunc():
    while True:
        time.sleep(5)
        print("Adding new object from thread")
        objects.append(Car())


t = Thread(target=myfunc)
t.start()

env = sim.Environment(trace=False)
env.animation_parameters(width=1, height=1)
env.animate = True
Car()
env.run(till=100)
