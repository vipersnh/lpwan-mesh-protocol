from pdb import set_trace
import salabim as sim
from time import sleep
import _thread
import socket
import interface_wrapper as intf
from servernode import *
from servertimer import *
from serverthread import *
from serverphy import *
from interface import send_message, recv_message

g_nodes_dict = {}

class server_t:
    def __init__(self, cpp_server):
        print("server_t : __init__ called")
        self.server_socket_fd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket_fd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket_fd.bind(('localhost', intf.SERVER_PORT))
        self.server_socket_fd.listen(100000)
        self.cpp_server = cpp_server
        self.node_id_ptr = intf.new_intp()
        self.instance_id_ptr = intf.new_intp()
    
    def get_request_information(self):
        messenger_socket_fd, _ = self.server_socket_fd.accept()
        message = recv_message(messenger_socket_fd)
        self.cpp_server.copy_received_message(message)
        msg_num = self.cpp_server.get_request_information(self.node_id_ptr, self.instance_id_ptr)
        node_id = intf.intp_value(self.node_id_ptr)
        instance_id = intf.intp_value(self.instance_id_ptr)
        return messenger_socket_fd, msg_num, node_id, instance_id

    def process(self):
        while True:
            [sock, msg_num, node_id, instance_id] = self.get_request_information()

            if node_id not in g_nodes_dict.keys():
                g_nodes_dict[node_id] = servernode_t(node_id)
            node = g_nodes_dict[node_id]

            if msg_num==intf.MESSENGER_THREAD_REGISTER_CMD:
                serverthread = serverthread_t(sock, node_id, instance_id, node)
                node.add_node_thread(serverthread, instance_id)
            elif msg_num==intf.MESSENGER_TIMER_REGISTER_CMD:
                servertimer = servertimer_t(sock, node_id, instance_id, node)
                node.add_node_timer(servertimer, instance_id)
            elif msg_num==intf.MESSENGER_PHY_REGISTER_CMD:
                serverphy = serverphy_t(sock, node_id, instance_id, node)
                node.set_server_phy(serverphy)
            else:
                print("server->process : Received msg_num = {0}".format(msg_num))
                assert 0


def server_thread_fn():
    cpp_server = intf.server_t()
    server = server_t(cpp_server)
    while True:
        server.process()

