from pdb import set_trace
import interface_wrapper as intf


def send_message(sock, message, message_len):
    sock.sendall(message[0:message_len])

def recv_message(sock):
    data = bytes()
    length_field = 0
    while True:
        # Receive length field
        if length_field==0:
            data += sock.recv(4 - len(data))
            if len(data) == 4:
                length_field = 0;
                length_field |= data[0] << 0
                length_field |= data[1] << 8
                length_field |= data[2] << 16
                length_field |= data[3] << 24
        else:
            if len(data) == (4 + length_field):
                return bytearray(data)
            else:
                data += sock.recv(4 + length_field - len(data))
