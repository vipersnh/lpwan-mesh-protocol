from pdb import set_trace
import interface_wrapper
import threading
import salabim as sim
from server import server_thread_fn
import time
import subprocess
import traceback
import sys

env = sim.Environment(trace=False)

t = threading.Thread(target=server_thread_fn, args=())
t.start()

time.sleep(1)

num_nodes = 1

# start individual node applications
node_processes = []
log_files = []
for node_idx in range(num_nodes):
    log_file = open("./op_files/op_log_node_{0}.txt".format(node_idx), "w", 10)
    process = subprocess.Popen(["./node_application.elf", "-n", str(node_idx)], stdout=log_file)
    node_processes.append(process)

duration = 100
simulation_speed = 1
env.animate = False
env.animation_parameters(width=1, height=1, speed=simulation_speed)
print("starting simulator now")
env.run(duration)
print("simulation completed")

# kill indivial node applications since the simulation is over
for process in node_processes:
    process.terminate()

