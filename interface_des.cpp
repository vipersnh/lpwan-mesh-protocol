#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <netinet/tcp.h>
#include <mutex>
#include <sys/ioctl.h>
#include <sched.h>

using namespace std;

#define DES_INTERFACE
#include "interface.h"

/* Implementation of serverbase_t */

/* ----> public members */

int serverbase_t::get_node_id(void) {
    return this->node_id;
}

int serverbase_t::get_instance_id(void) {
    return this->instance_id;
}

message_enum_t serverbase_t::get_recv_message_num(void) {
    message_enum_t msg_num;
    memcpy((char*)&msg_num, this->message_buffer + INTF_LEN_FIELD_SIZE, sizeof(msg_num));
    return msg_num;
}

void serverbase_t::send_rsp_status(message_enum_t msg_num, bool result) {
    messenger_status_t status = {
        .result = result,
    };
    this->send_message(msg_num, (char*)&status, sizeof(status));
}

float serverbase_t::get_req_sleep_duration(void) {
    messenger_sleep_cmd_struct_t *ptr = (messenger_sleep_cmd_struct_t *) 
        (this->message_buffer + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE);
    return ptr->duration;
}

void serverbase_t::send_rsp_current_time(float time) {
    this->send_message(MESSENGER_RSP_GET_TIME_CMD, (char*)&time, sizeof(time));    
}

void serverbase_t::copy_received_message(char *buffer, int buffer_len) {
    assert (buffer_len > 0);
    assert (buffer_len <= (int)sizeof(this->message_buffer));
    memcpy(this->message_buffer, buffer, buffer_len);
}

int serverbase_t::copy_sending_message(char *buffer, int buffer_len) {
    assert (buffer_len >= this->message_len);
    memcpy(buffer, this->message_buffer, this->message_len);
    return this->message_len;
}

void serverbase_t::send_message(message_enum_t msg_num, char *buffer, int buffer_len) {
    int len = sizeof(msg_num) + buffer_len;
    memcpy(this->message_buffer, (char*)&len, INTF_LEN_FIELD_SIZE);
    memcpy(this->message_buffer + INTF_LEN_FIELD_SIZE, (char*)&msg_num, INTF_MESSAGE_ENUM_FIELD_SIZE);
    memcpy(this->message_buffer + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE, buffer, buffer_len);
    this->message_len = INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE + buffer_len;
}


/* Implementation of serverthread_t */

serverthread_t::serverthread_t(int node_id, int thread_id) {
    this->node_id = node_id;
    this->instance_id = thread_id;

    //printf("serverthread->serverthread_t : Called with node_id %d, thread_id %d\n", node_id, thread_id);
}

int serverthread_t::get_thread_id(void) {
    return this->instance_id;
}

int serverthread_t::get_req_timer_id(void) {
    int timer_id;
    memcpy((char*)&timer_id, this->message_buffer 
            + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE, sizeof(timer_id));
    return timer_id;
}

radio_tx_packet_t * serverthread_t::get_tx_packet(char *buffer, int buffer_len) {
    assert (buffer_len >= (int)sizeof(radio_tx_packet_t));
    memcpy(buffer, this->message_buffer + INTF_LEN_FIELD_SIZE
            + INTF_MESSAGE_ENUM_FIELD_SIZE, sizeof(radio_tx_packet_t));
    return (radio_tx_packet_t*)buffer;
}

int serverthread_t::get_event_id(void) {
    int event_id;
    memcpy((char*)&event_id, this->message_buffer
            + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE, sizeof(event_id));
    return event_id;
}

messenger_event_struct_t * serverthread_t::get_messenger_event_struct(char *buffer, int buffer_len) {
    assert (buffer_len >= MAX_MESSAGE_SIZE);
    memcpy(buffer, this->message_buffer + INTF_LEN_FIELD_SIZE 
            + INTF_MESSAGE_ENUM_FIELD_SIZE, sizeof(messenger_event_struct_t));
    return (messenger_event_struct_t*)buffer;
}

void serverthread_t::send_messenger_event_struct(bool result, messenger_event_struct_t *event_struct) {
    messenger_status_t status = { .result = result };
    char buffer[sizeof(messenger_status_t) + sizeof(messenger_event_struct_t)];
    memcpy(buffer, (char*)&status, sizeof(status));
    memcpy(buffer + sizeof(messenger_status_t), (char*)event_struct, sizeof(messenger_event_struct_t));
    this->send_message(MESSENGER_RSP_WAIT_FOR_EVENT_CMD, buffer, sizeof(buffer));
}

radio_rx_enable_struct_t * serverthread_t::get_rx_enable_struct(void) {
    radio_rx_enable_struct_t * ptr;
    ptr = (radio_rx_enable_struct_t *)(this->message_buffer + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE);
    return ptr;
}

void serverthread_t::send_rx_packet_rsp(bool result, radio_rx_packet_t *packet) {
    messenger_status_t status = { .result = result };
    if (result) {
        char buffer[sizeof(messenger_status_t) + sizeof(radio_rx_packet_t)];
        memcpy(buffer, (char*)&status, sizeof(status));
        memcpy(buffer + sizeof(messenger_status_t), packet, sizeof(radio_rx_packet_t));
        this->send_message(MESSENGER_RSP_RX_PACKET_CMD, buffer, sizeof(buffer));
    } else {
        this->send_rsp_status(MESSENGER_RSP_RX_PACKET_CMD, false);
    }
}

/* Implementation of servertimer_t */

servertimer_t::servertimer_t(int node_id, int timer_id) {
    this->node_id = node_id;
    this->instance_id = timer_id;

    //printf("servertimer->servertimer_t : Called with node_id %d, timer_id %d\n", node_id, timer_id);
}

int servertimer_t::get_timer_id(void) {
    return this->instance_id;
}

/* Implementation of serverphy_t */

serverphy_t::serverphy_t(int node_id, int phy_id) {
    this->node_id = node_id;
    this->instance_id = phy_id;

    //printf("serverphy->serverphy_t : Called with node_id %d, phy_id %d\n", node_id, phy_id);
}

/* Implementation of server_t */

/* ----> public members */

void server_t::copy_received_message(char *buffer, int buffer_len) {
    assert (buffer_len > 0);
    assert (buffer_len <= (int)sizeof(this->message_buffer));
    memcpy(this->message_buffer, buffer, buffer_len);
}

int server_t::copy_sending_message(char *buffer, int buffer_len) {
    assert (buffer_len >= this->message_len);
    memcpy(buffer, this->message_buffer, this->message_len);
    return this->message_len;
}

message_enum_t server_t::get_request_information(int *node_id, int *instance_id) {
    message_enum_t recv_msg;
    messenger_register_struct_t register_cmd;

    assert (node_id != NULL);
    assert (instance_id != NULL);

    memcpy((char*)&recv_msg, this->message_buffer + INTF_LEN_FIELD_SIZE, INTF_MESSAGE_ENUM_FIELD_SIZE);

    memcpy((char*)&register_cmd, this->message_buffer + INTF_LEN_FIELD_SIZE + INTF_MESSAGE_ENUM_FIELD_SIZE, sizeof(register_cmd));

    *node_id = register_cmd.node_id;
    *instance_id = register_cmd.instance_id;

    return recv_msg;
}


