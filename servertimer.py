from pdb import set_trace
import salabim as sim
import interface_wrapper as intf
from interface import recv_message, send_message

class servertimer_t(sim.Component):
    def __init__(self, sock, node_id, timer_id, servernode):
        super(servertimer_t, self).__init__()
        self.messenger_sock = sock
        self.node_id = node_id
        self.timer_id = timer_id
        self.cpp_node_timer = intf.servertimer_t(node_id, timer_id)
        self.servernode = servernode
        self.message_buffer = bytearray(intf.MAX_MESSAGE_SIZE)
        #print("servertimer->__init__ : Created with node_id {0}, timer_id {1} at {2}" .format(self.node_id, self.timer_id, self.env.now()))
        self.wait_state = sim.State('start-wait')
        self.acknowledge_registration(True)
        self.is_running = False
    
    def send_message(self):
        message_len = self.cpp_node_timer.copy_sending_message(self.message_buffer)
        send_message(self.messenger_sock, self.message_buffer, message_len)

    def acknowledge_registration(self, status):
        self.cpp_node_timer.send_rsp_status(intf.MESSENGER_TIMER_REGISTER_RSP, status)
        self.send_message()

    def get_recv_message_num(self):
        message = recv_message(self.messenger_sock)
        self.cpp_node_timer.copy_received_message(message)
        return self.cpp_node_timer.get_recv_message_num()

    def trigger_timer_start(self):
        assert self.is_running==False
        self.is_running = True
        self.wait_state.set()

    def trigger_timer_stop(self):
        assert self.is_running==True
        self.wait_state.reset()
        self.is_running = False

    def process(self):
        while True:
            yield self.wait(self.wait_state)
            while self.is_running:
                msg_num = self.get_recv_message_num()
                if msg_num==intf.MESSENGER_REQ_SLEEP_CMD:
                    duration = self.cpp_node_timer.get_req_sleep_duration()
                    yield self.hold(duration)
                    self.cpp_node_timer.send_rsp_status(intf.MESSENGER_RSP_SLEEP_CMD, True)
                elif msg_num==intf.MESSENGER_REQ_GET_TIME_CMD:
                    self.cpp_node_timer.send_rsp_current_time(self.env.now())
                else:
                    print("Received msg_num = {0}".format(msg_num))
                    assert 0
                self.send_message()
                

