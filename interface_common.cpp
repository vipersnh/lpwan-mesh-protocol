#include <thread>
#include <mutex>
#include <assert.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/select.h>
#include "interface.h"

using namespace std;

/* malloc and free functions for radio_tx_packet_t and radio_rx_packet_t */

radio_rx_packet_t * malloc_radio_rx_packet_copy_tx(radio_tx_packet_t *tx_packet,
        float rx_power, float rx_noise, float rx_delay) {
    radio_rx_packet_t *rx_packet = (radio_rx_packet_t*) calloc(1, sizeof(radio_rx_packet_t));
    assert(rx_packet);
    memcpy(rx_packet->packet, tx_packet->packet, tx_packet->packet_len);
    rx_packet->packet_len = tx_packet->packet_len;
    rx_packet->freq = tx_packet->freq;
    rx_packet->channel = tx_packet->channel;
    rx_packet->rx_rate = tx_packet->tx_rate;
    rx_packet->rx_power = rx_power;
    rx_packet->rx_noise = rx_noise;
    rx_packet->rx_delay = rx_delay;
    rx_packet->next = false;
    return rx_packet;
}

void free_radio_rx_packet_copy(radio_rx_packet_t *packet) {
    assert(packet);
    free(packet);
}

void print_hex_bytes(const char *tag, char *bytes, int count) {
    printf("\nTag : %s : ",tag);
    if (count==0) {
        printf("print_hex_bytes : count = %d\n", count);
    }
    for (int i=0; i<count; i++) {
        printf("0x%x, ", bytes[i]);
    }
}

void send_message_common(
        int socket_fd, 
        mutex *lock_mutex, 
        message_enum_t msg_num, 
        char *storage_buffer, 
        int storage_buffer_len, 
        char * buffer, 
        int buffer_len) {
    lock_mutex->lock();

    int total_bytes_written = 0;
    int len = sizeof(message_enum_t) + buffer_len, write_len;

    /* Write the length information */
    *(int*)(storage_buffer + total_bytes_written) = len;
    total_bytes_written += sizeof(int);

    /* Write the message_enum_t information */
    *(message_enum_t*)(storage_buffer + total_bytes_written) = msg_num;
    total_bytes_written += sizeof(message_enum_t);

    /* Write the message-data information */
    if (buffer && buffer_len) {
        memcpy(storage_buffer + total_bytes_written, buffer, buffer_len);
        total_bytes_written += buffer_len;
    }

    write_len = 0;
    while (write_len < total_bytes_written) {
        write_len += send(socket_fd, storage_buffer + write_len, 
                total_bytes_written - write_len, 0);
    }

#ifdef DEBUG_BUILD
    printf("\n\nsend_message_common : Bytes");
    print_hex_bytes("len-field", storage_buffer, sizeof(int));
    print_hex_bytes("msg-enum", storage_buffer + sizeof(int), sizeof(message_enum_t));
    print_hex_bytes("msg-data", storage_buffer + sizeof(int) + sizeof(message_enum_t), 
            total_bytes_written - sizeof(int) - sizeof(message_enum_t));
    printf("\nsend_message_common : enum = %d, len-field = %d, buffer_len = %d\n\n", 
            msg_num, len, buffer_len);
#endif

    lock_mutex->unlock();
}

bool recv_message_common(
        int socket_fd, 
        mutex *lock_mutex, 
        message_enum_t *msg_num, 
        char *storage_buffer, 
        int storage_buffer_len, 
        char * buffer, 
        int buffer_len) {

    fd_set readset;
    int total_bytes_read = 0, read_len;
    int len, res;

    lock_mutex->lock();

    /* Read the length information */
    read_len = 0;
    while (read_len < (int)sizeof(int)) {
        do {
            FD_ZERO(&readset);
            FD_SET(socket_fd, &readset);
            res = select(socket_fd + 1, &readset, NULL, NULL, NULL);
        } while (res == -1 && errno == EINTR && res == 0);

        res = recv(socket_fd, storage_buffer + total_bytes_read,
               sizeof(int) - read_len, 0);
        if (res != 0 && res != -1) {
            read_len += res;
            total_bytes_read += res;
        }

        if ( res==0 ) {
            printf("\n Socket closed\n");
            exit(EXIT_FAILURE);
        }
    }


    /* Read the information indicated by the length */
    len = *(int*)storage_buffer;

    read_len = 0;
    while (read_len < len) {
        do {
            FD_ZERO(&readset);
            FD_SET(socket_fd, &readset);
            res = select(socket_fd + 1, &readset, NULL, NULL, NULL);
        } while (res == -1 && errno == EINTR);

        res = recv(socket_fd, storage_buffer + total_bytes_read,
               len - read_len, 0);
        if (res != 0 && res != -1) {
            read_len += res;
            total_bytes_read += res;
        }

        if ( res==0 ) {
            printf("\n Socket closed\n");
            exit(EXIT_FAILURE);
        }
    }

    /* Decode information availabe into message_enum_t and message data */
    *msg_num = *(message_enum_t*)(storage_buffer + sizeof(int));

#ifdef DEBUG_BUILD
    /* Print the obtained information */
    printf("\n\nrecv_message_common : Bytes");
    print_hex_bytes("len-field", storage_buffer, sizeof(int));
    print_hex_bytes("msg-enum", storage_buffer + sizeof(int), sizeof(message_enum_t));
    print_hex_bytes("msg-data", storage_buffer + sizeof(int) + sizeof(message_enum_t), 
            total_bytes_read - sizeof(int) - sizeof(message_enum_t));
    printf("\nrecv_message_common : enum = %d, len-field = %d, buffer_len = %ld\n\n", 
            *msg_num, len, total_bytes_read - sizeof(int) - sizeof(message_enum_t));
#endif

    if (buffer && buffer_len) {
        assert (total_bytes_read >= (int)(sizeof(int) + sizeof(message_enum_t)));
        assert (buffer_len >= (int)(total_bytes_read - sizeof(int) - sizeof(message_enum_t)));
        memcpy(buffer, storage_buffer + sizeof(int) + sizeof(message_enum_t),
                total_bytes_read - sizeof(int) - sizeof(message_enum_t));
    }

    lock_mutex->unlock();
    return true;
}

