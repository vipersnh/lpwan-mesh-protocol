CXX := /usr/local/opt/gcc/bin/g++-7
all : example_app node_app swig_app

PYTHON_INC :=/usr/local/Cellar/python/3.6.4_4/Frameworks/Python.framework/Versions/3.6/include/python3.6m
PYTHON_LIB :=/usr/local/Cellar/python/3.6.4_4/Frameworks/Python.framework/Versions/3.6/lib

#CFLAGS := -DDEBUG_BUILD

COMMON_NODE_SOURCES := interface_common.cpp interface_nodes.cpp 

SOURCES_EXAMPLE_APPLICATION := ${COMMON_NODE_SOURCES} example_application.cpp

example_app : ${SOURCES_EXAMPLE_APPLICATION}
	$(eval SOURCES := ${SOURCES_EXAMPLE_APPLICATION})
	$(eval TARGET := example_application.elf )
	@echo "Compiling ${TARGET}" 
	@echo "CXX -Wall -I./ ${SOURCES} -o ${TARGET}"
	@${CXX} -Wall -I./ ${CFLAGS} ${SOURCES} -o ${TARGET}
	@echo "\n"

SOURCES_NODE_APPLICATION := ${COMMON_NODE_SOURCES} ./node_application/node_application.cpp ./node_application/slot_handler.cpp
SOURCES_NODE_APPLICATION += ./node_application/beacon_tx_scheduler.cpp

node_app : ${SOURCES_NODE_APPLICATION}
	$(eval SOURCES := ${SOURCES_NODE_APPLICATION})
	$(eval TARGET := node_application.elf )
	@echo "Compiling ${TARGET}" 
	@echo "CXX -Wall -I./ ${SOURCES} -o ${TARGET}"
	@${CXX} -Wall -I./ ${CFLAGS} ${SOURCES} -o ${TARGET}
	@echo "\n"

swig_app :
	$(eval SOURCES := interface_wrap.cxx interface_des.cpp interface_common.cpp )
	$(eval TARGET := _interface_wrapper.so )
	@echo "Compiling ${TARGET}" 
	swig -python -c++ interface.swig
	@${CXX} -Wall -shared -I${PYTHON_INC} -L${PYTHON_LIB} -lpython3.6m ${SOURCES} -o ${TARGET}
	@echo "CXX -Wall -shared -I${PYTHON_INC} -L${PYTHON_LIB} -lpython3.6m ${SOURCES} -o ${TARGET}"
	@echo "\n"

