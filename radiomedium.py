import sys
import salabim as sim
from pdb import set_trace
import interface_wrapper as intf
from collections import namedtuple
from math import sqrt, log10, pi

# This file simulates a multi channel radio medium with the following assumptions and rules
#   1. Different radio channels do not interfere with each other
#   2. Multiple transmissions collide with each other and result in noise for the receiver 
#      by the other transmission
#   3. A node calls speak to transmit its packet on a channel, then calls unspeak to end the 
#      transmission on the channel
#   4. A node calls listen on a channel to listen on that channel for packets, and the channel calls
#      node's handle_received_packet when a packet is received on that channel with the phy_packet,
#      channel_number, rx_power and noise_power

radiolisten_t = namedtuple("radiolisten_t", ["t_start"])

class radiocalc_t:
    def __init__(self, frequency_band, path_loss_exponent):
        self.param_freq_band = frequency_band
        if frequency_band==intf.RADIO_FREQ_433MHz:
            self.param_freq = 433*1E6
        elif frequency_band==intf.RADIO_FREQ_868MHz:
            self.param_freq = 868*1E6
        elif frequency_band==intf.RADIO_FREQ_915MHz:
            self.param_freq = 915*1E6
        elif frequency_band==intf.RADIO_FREQ_2400MHz:
            self.param_freq = 2400*1E6
        else:
            print("energy_base->calc_required_power : Invalid frequency_band specified {0}".format(frequency_band));
            assert 0
        self.param_path_loss_exponent = path_loss_exponent

    def calc_path_gain(self, distance_m):
        # Calculate the required path loss using Friis transmission formula
        wavelength = 3*1E8/self.param_freq
        path_loss = 10*log10(pow(wavelength/(4*pi), 2)/pow(distance_m, self.param_path_loss_exponent))
        return path_loss

    def calc_destination_uplink_power(self, uplink_power, uplink_gain_dBi, downlink_gain_dBi, distance_m):
        path_gain = self.calc_path_gain(distance_m)
        path_gain = path_gain + uplink_gain_dBi + downlink_gain_dBi
        return uplink_power + path_gain

    def calc_mW_from_dBm(self, dBm):
        return pow(10, dBm/10)

    def calc_dBm_from_mW(self, mW):
        return 10*log10(mW)
        

class radiochannel_t (sim.Component):
    def __init__(self, radio_band, channel, radiocalc):
        super(radiochannel_t, self).__init__()
        self.param_radio_band = radio_band
        self.param_channel = channel
        self.transmission_packets = list()
        self.listening_nodes = dict()
        self.radiocalc = radiocalc

    def speak(self, phy_packet):
        self.transmission_packets.append(phy_packet)

    def calc_rx_power(self, phy_location_1, phy_location_2, tx_power):
        x_del = phy_location_1.x - phy_location_2.x
        y_del = phy_location_1.y - phy_location_2.y
        z_del = phy_location_1.z - phy_location_2.z
        distance_m = sqrt(x_del*x_del + y_del*y_del + z_del*z_del)
        return self.radiocalc.calc_destination_uplink_power(tx_power, 2, 2, distance_m)

    def unspeak(self, phy_packet):
        noise_power = 0
        self.transmission_packets.remove(phy_packet)
        tx_radiophy = phy_packet.radiophy
        for rx_radiophy, radiolisten in self.listening_nodes.items():
            if len(self.transmission_packets):
                for packet_item in self.transmission_packets:
                    rx_power_dBm = self.calc_rx_power(packet_item.radiophy.phy_location, rx_radiophy.phy_location, packet_item.tx_power)
                    noise_power += self.radiocalc.calc_mW_from_dBm(rx_power_dBm)
            # Check if the node was registered before the packet transmission was scheduled, if so then
            # then call the packet handler from that node
            if phy_packet.t_start > radiolisten.t_start:
                rx_power = self.calc_rx_power(tx_radiophy.phy_location, rx_radiophy.phy_location, phy_packet.tx_power)
                rx_radiophy.handle_received_packet(phy_packet, self, rx_power, noise_power)

    def listen(self, radiophy):
        start_time = self.env.now()
        radiolisten = radiolisten_t(t_start = self.env.now())
        self.listening_nodes[radiophy] = radiolisten

    def unlisten(self, radiophy):
        del self.listening_nodes[radiophy]

class radiomedium_t (sim.Component):
    def __init__(self):
        self.is_initialized = False

    def init(self, radio_band, channels_list, frequency_band, path_loss_exponent):
        super(radiomedium_t, self).__init__()
        self.radio_band = radio_band
        self.is_initialized = True
        self.radiocalc = radiocalc_t(frequency_band, path_loss_exponent)

        self.channels = dict()
        for channel in channels_list:
            self.channels[channel] = radiochannel_t(self.radio_band, channel, self.radiocalc)
        
    def get_radio_channel(self, channel):
        return self.channels[channel]

g_radio_medium = radiomedium_t()
