#include <stdint.h>
#include <stdio.h>
#include <thread>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "interface.h"

#include "types.h"
#include "slot_handler.h"
#include "beacon_tx_scheduler.h"

typedef enum {
    MAIN_THREAD_ID,
    SLOT_HANDLER_THREAD_ID,
    BEACON_TX_SCHEDULER_THREAD_ID,
    TESTING_THREAD_ID,
} thread_id_enum_t;

typedef enum {
    TEST_EVENT0_ID,
} event_id_enum_t;

void testing_thread_fn(nodethread_t *nodethread) {
    messenger_event_struct_t event_struct;
    while (true) {
        printf("testing_thread_fn : Wait for TEST_EVENT0_ID\n");
        nodethread->wait_for_event(TEST_EVENT0_ID, &event_struct);
        printf("testing_thread_fn : woken beacuase of TEST_EVENT0_ID with buffer_len = %d at %f\n", event_struct.buffer_len, nodethread->get_time());
        memset((char*)&event_struct, 0x00, sizeof(messenger_event_struct_t));
    }
}

int main(int argc, char *argv[]) {
    int node_id = -1;
    int opt;

    beacon_periods_enum_t beacon_period = BEACON_PERIOD_SLOTS_30;

    setbuf(stdout, NULL);

    while ((opt = getopt(argc, argv, "n:")) != -1) {
        switch (opt) {
        case 'n':
            node_id = atoi(optarg);
            break;
        default: /* '?' */
            exit(EXIT_FAILURE);
        }
    }

    if (node_id==-1) {
        printf("main : Error - invalid node-id passed\n");
        exit(EXIT_FAILURE);
    } else {
        printf("main : starting node_application with node-id %d\n", node_id);
    }

    //if (node_id==0) { g_is_master_node = true; }

    nodephy_t *nodephy = new nodephy_t(node_id);
    nodethread_t *main_thread = new nodethread_t(node_id, MAIN_THREAD_ID, NULL);
    main_thread->start();
    messenger_event_struct_t event_struct = {
        .event_id = TEST_EVENT0_ID,
        .buffer_len = 25,
        .buffer = {0,1,2,3},
    };

    nodethread_t *test_thread = new nodethread_t(node_id, TESTING_THREAD_ID, &testing_thread_fn);
    test_thread->start();

    /* initialize and start the slot handler task */
    init_slot_handler(node_id, SLOT_HANDLER_THREAD_ID, BEACON_PERIOD_SLOTS_30, nodephy);

    /* initialize and start the beacon tx scheduler task */
    init_beacon_tx_scheduler(node_id, BEACON_TX_SCHEDULER_THREAD_ID, beacon_period, RADIO_CHANNEL_0);

    while (true) {
        main_thread->sleep(10);
        main_thread->activate_event(&event_struct);
    }

    return 0;
}


