#ifndef __SLOT_HANDLER_H__
#define __SLOT_HANDLER_H__

typedef struct {
    bool is_rx_enabled;
    bool is_beacon_slot;

    radio_freq_enum_t rx_freq;
    radio_channel_enum_t rx_channel;

    radio_tx_packet_t *tx_authentication_packet;
    radio_tx_packet_t *tx_router_packet;
    radio_tx_packet_t *tx_data_packet;
    radio_tx_packet_t *tx_beacon_packet;

    bool tx_authentication_ignored;
    bool tx_router_ignored;
    bool tx_data_ignored;
    bool tx_beacon_ignored;

    radio_rx_packet_t *rx_beacon_packet;
    radio_rx_packet_t *rx_authentication_packet;
    radio_rx_packet_t *rx_router_packet;
    radio_rx_packet_t *rx_data_packet;
} slot_t;

class slot_handler_t {
    private:
        slot_t *all_slots;
        int num_slots;
        int curr_slot_num;
        mutex mlock;
        nodephy_t *nodephy;

        float slot_time_offset;
        bool slot_time_offset_enabled;
    
    public:
        slot_handler_t(int num_slots, nodephy_t *nodephy);
        void process(nodethread_t *nodethread);

    public:
        void add_slot_time_offset(float time_delay);

        bool add_beacon_packet(int slot_no, radio_tx_packet_t *);
        bool add_authentication_packet(int slot_no, radio_tx_packet_t *);
        bool add_router_packet(int slot_no, radio_tx_packet_t *);
        bool add_data_packet(int slot_no, radio_tx_packet_t *);

        bool enable_rx_slot(int slot_no, radio_freq_enum_t freq, radio_channel_enum_t channel);

        bool get_beacon_packet(int slot_no, radio_rx_packet_t *);
        bool get_authentication_packet(int slot_no, radio_rx_packet_t *);
        bool get_router_packet(int slot_no, radio_rx_packet_t *);
        bool get_data_packet(int slot_no, radio_rx_packet_t *);
};

extern slot_handler_t * g_slot_handler;
extern nodethread_t * g_slot_handler_nodethread;
extern void init_slot_handler(int node_id, int thread_id, int num_slots, nodephy_t *nodephy);

#endif
