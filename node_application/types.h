#ifndef __NODE_APPLICATION_H__
#define __NODE_APPLICATION_H__

using namespace std;

typedef enum {
    PACKET_TYPE_BEACON_MSG,
    PACKET_TYPE_AUTHENTICATION_MSG,
    PACKET_TYPE_ROUTER_MSG,
    PACKET_TYPE_DATA_MSG,
} packet_type_enum_t;

#define SLOT_PERIOD_MS          500    /* This indicates the full slot duration */
#define SLOT_PERIOD_TX_MS       400    /* This indicates the full slot duration required for packet transmission */
#define SLOT_START_GUARD_MS      50 
#define SLOT_STOP_GUARD_MS       50 

typedef struct {
    packet_type_enum_t packet_type;
} packet_hdr_t;

typedef enum {
    BEACON_PERIOD_SLOTS_30  =  30,
    BEACON_PERIOD_SLOTS_60  =  60,
    BEACON_PERIOD_SLOTS_120 = 120,
    BEACON_PERIOD_SLOTS_240 = 240,
    BEACON_PERIOD_SLOTS_480 = 480,
    BEACON_PERIOD_SLOTS_960 = 960,
} beacon_periods_enum_t;

typedef struct {
    unsigned int node_idx;
    unsigned int beacon_message_period;
} beacon_message_struct_t;

typedef struct {
    int beacon_message_period;
    int beacon_start_slot;
    radio_channel_enum_t beacon_channel;
    float beacon_rx_power_dB;
} neighbor_node_t;

#endif
