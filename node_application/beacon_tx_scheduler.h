#ifndef __BEACON_TX_SCHEDULER_H__
#define __BEACON_TX_SCHEDULER_H__

using namespace std;

class beacon_tx_scheduler_t {
    private:
        beacon_periods_enum_t period;
        radio_channel_enum_t channel;
        slot_handler_t *slot_handler;
    public:
        beacon_tx_scheduler_t(beacon_periods_enum_t period, radio_channel_enum_t channel);
        void process(nodethread_t *nodethread);

        /* TODO : Additional functions for changing or handling of changes to beacon scheduler */
};

extern beacon_tx_scheduler_t * g_beacon_tx_scheduler;
extern void init_beacon_tx_scheduler(int node_id, int thread_id, beacon_periods_enum_t period, radio_channel_enum_t channel);

#endif
