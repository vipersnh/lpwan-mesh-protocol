#include "interface.h"
#include "types.h"
#include "assert.h"
#include "string.h"
#include "slot_handler.h"
#include "beacon_tx_scheduler.h"

beacon_tx_scheduler_t * g_beacon_tx_scheduler = NULL;

static nodethread_t * g_beacon_tx_scheduler_thread = NULL;

void beacon_tx_scheduler_fn(nodethread_t *nodethread) {
    assert (g_beacon_tx_scheduler != NULL);
    g_beacon_tx_scheduler->process(nodethread);
}

void init_beacon_tx_scheduler(int node_id, int thread_id, beacon_periods_enum_t period, radio_channel_enum_t channel) {
    g_beacon_tx_scheduler = new beacon_tx_scheduler_t(period, channel);
    g_beacon_tx_scheduler_thread = new nodethread_t(node_id, thread_id, &beacon_tx_scheduler_fn);
    g_beacon_tx_scheduler_thread->start();
}

beacon_tx_scheduler_t::beacon_tx_scheduler_t(beacon_periods_enum_t period, radio_channel_enum_t channel) {
    this->period = period;
    this->channel = channel;
}

void beacon_tx_scheduler_t::process(nodethread_t *nodethread) {
    radio_tx_packet_t *tx_packet;
    while (true) {
        tx_packet = new radio_tx_packet_t;

        memset(tx_packet, 0x00, sizeof(radio_tx_packet_t));
        memset(tx_packet->packet, 0xF1, sizeof(tx_packet->packet));
        tx_packet->packet_len = sizeof(tx_packet->packet);
        tx_packet->tx_rate = DATA_RATE_SF_08;
        tx_packet->tx_power = 0; /* dBm */
        tx_packet->freq = RADIO_FREQ_433MHz; /* TODO: Change it */
        tx_packet->channel = this->channel;
  
        g_slot_handler->add_beacon_packet(0, tx_packet);
        nodethread->sleep(this->period * SLOT_PERIOD_MS/1000.0);
    }
}
