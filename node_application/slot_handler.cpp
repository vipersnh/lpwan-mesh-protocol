#include "interface.h"
#include "types.h"
#include "assert.h"
#include "string.h"
#include "slot_handler.h"

slot_handler_t * g_slot_handler = NULL;
nodethread_t * g_slot_handler_nodethread = NULL;

void slot_handler_fn(nodethread_t *nodethread) {
    assert (g_slot_handler != NULL);
    g_slot_handler->process(nodethread);
}

void init_slot_handler(int node_id, int thread_id, int num_slots, nodephy_t *nodephy) {
    g_slot_handler = new slot_handler_t(num_slots, nodephy);
    g_slot_handler_nodethread = new nodethread_t(node_id, thread_id, &slot_handler_fn);
    g_slot_handler_nodethread->start();
}

slot_handler_t::slot_handler_t(int num_slots, nodephy_t *nodephy) {
    this->num_slots = num_slots;
    this->all_slots = new slot_t[num_slots];
    slot_t *slot_ptr;
    for (int i=0; i<this->num_slots; i++) {
        slot_ptr = this->all_slots + i;
        slot_ptr->is_rx_enabled = false;
        slot_ptr->is_beacon_slot = false;
        slot_ptr->tx_beacon_packet = slot_ptr->tx_authentication_packet = slot_ptr->tx_router_packet = slot_ptr->tx_data_packet = NULL;
        slot_ptr->rx_beacon_packet = slot_ptr->rx_authentication_packet = slot_ptr->rx_router_packet = slot_ptr->rx_data_packet = NULL;
    }
    this->nodephy = nodephy;
    this->curr_slot_num = 0;
}

void slot_handler_t::process(nodethread_t *nodethread) {
    radio_tx_packet_t * tx_packet;
    radio_rx_packet_t rx_packet, *rx_packet_ptr;
    packet_hdr_t *packet_hdr;
    slot_t *slot_ptr;
    radio_rx_enable_struct_t rx_enable_struct;
    while (true) {
        if (this->slot_time_offset_enabled) {
            this->slot_time_offset_enabled = false;
            nodethread->sleep(this->slot_time_offset);
        }

        printf("slot_handler->process : current-slot %d at %f\n", this->curr_slot_num, nodethread->get_time());

        /* Check for received packets from the previous slots and put them in previous slot */
        slot_ptr = this->all_slots + ((this->num_slots + this->curr_slot_num - 1) % this->num_slots);
        assert (slot_ptr->is_rx_enabled==false);
        do {
            if (this->nodephy->rx_packet_from_thread(&rx_packet, nodethread)) {
                rx_packet_ptr = new radio_rx_packet_t;
                memcpy((char*)rx_packet_ptr, (char*)&rx_packet, sizeof(radio_rx_packet_t));
                packet_hdr = (packet_hdr_t*) rx_packet_ptr;
                switch (packet_hdr->packet_type) {
                    case PACKET_TYPE_AUTHENTICATION_MSG: 
                        assert (slot_ptr->rx_authentication_packet==NULL);
                        slot_ptr->rx_authentication_packet = rx_packet_ptr;
                        break;
                    case PACKET_TYPE_ROUTER_MSG: 
                        assert (slot_ptr->rx_router_packet==NULL);
                        slot_ptr->rx_router_packet = rx_packet_ptr;
                        break;
                    case PACKET_TYPE_DATA_MSG: 
                        assert (slot_ptr->rx_data_packet==NULL);
                        slot_ptr->rx_data_packet = rx_packet_ptr;
                        break;
                    case PACKET_TYPE_BEACON_MSG: 
                        assert (slot_ptr->rx_beacon_packet==NULL);
                        slot_ptr->rx_beacon_packet = rx_packet_ptr;
                        break;
                }
            } else {
                break;
            }
        } while (true);

        /* Process things for the current slot & wait for next slot */

        tx_packet = NULL;
        slot_ptr = this->all_slots + this->curr_slot_num;
        slot_ptr->tx_authentication_ignored = slot_ptr->tx_router_ignored = slot_ptr->tx_data_ignored = slot_ptr->tx_beacon_ignored = false;
        if (slot_ptr->tx_authentication_packet) {
            tx_packet = slot_ptr->tx_authentication_packet;
            slot_ptr->tx_authentication_ignored = false;
            slot_ptr->tx_authentication_packet = NULL;
            printf("slot_handler->process : tx_authentication_packet in %d\n", this->curr_slot_num);
        }

        if (slot_ptr->tx_router_packet) {
            if (tx_packet) {
                /* tx_packet was selected from higher priority packet type 
                 * free the packet setup for transmission and set it as ignored
                 */
                free(slot_ptr->tx_router_packet); 
                slot_ptr->tx_router_packet = NULL;
                slot_ptr->tx_router_ignored = true;
            } else {
                tx_packet = slot_ptr->tx_router_packet;
                slot_ptr->tx_router_ignored = false;
                slot_ptr->tx_router_packet = NULL;
                printf("slot_handler->process : tx_router_packet in %d\n", this->curr_slot_num);
            }
        }

        if (slot_ptr->tx_data_packet) {
            if (tx_packet) {
                /* tx_packet was selected from higher priority packet type 
                 * free the packet setup for transmission and set it as ignored
                 */
                free(slot_ptr->tx_data_packet); 
                slot_ptr->tx_data_packet = NULL;
                slot_ptr->tx_data_ignored = true;
            } else {
                tx_packet = slot_ptr->tx_data_packet;
                slot_ptr->tx_data_ignored = false;
                slot_ptr->tx_data_packet = NULL;
                printf("slot_handler->process : tx_data_packet in %d\n", this->curr_slot_num);
            }
        }

        if (slot_ptr->tx_beacon_packet) {
            if (tx_packet) {
                /* tx_packet was selected from higher priority packet type 
                 * free the packet setup for transmission and set it as ignored
                 */
                free(slot_ptr->tx_beacon_packet); 
                slot_ptr->tx_beacon_packet = NULL;
                slot_ptr->tx_beacon_ignored = true;
            } else {
                tx_packet = slot_ptr->tx_beacon_packet;
                slot_ptr->tx_beacon_ignored = false;
                slot_ptr->tx_beacon_packet = NULL;
                printf("slot_handler->process : tx_beacon_packet in %d\n", this->curr_slot_num);
            }
        }

        if (tx_packet) {
            /* Transmit the selected packet and free the packet then */
            this->nodephy->tx_packet_from_thread(tx_packet, nodethread);
            free(tx_packet); tx_packet = NULL;
        }

        if (slot_ptr->is_rx_enabled) {
            rx_enable_struct.freq = slot_ptr->rx_freq; rx_enable_struct.channel = slot_ptr->rx_channel;
            this->nodephy->rx_enable_from_thread(&rx_enable_struct, nodethread);
            slot_ptr->is_rx_enabled = false;
        }

        nodethread->sleep(SLOT_PERIOD_MS/1000.0);

        this->curr_slot_num += 1; this->curr_slot_num %= this->num_slots;
    }
}

void slot_handler_t::add_slot_time_offset(float time_delay) {
    this->slot_time_offset = time_delay;
    this->slot_time_offset_enabled = true;
}

bool slot_handler_t::add_beacon_packet(int slot_no, radio_tx_packet_t *packet) {
    assert (slot_no <= this->num_slots);
    slot_t *slot = this->all_slots + slot_no;
    printf("slot_handler->add_beacon_packet : slot_no = %d\n", slot_no);
    assert (slot->tx_beacon_packet==NULL);
    slot->tx_beacon_packet = packet;
    return true;
}
