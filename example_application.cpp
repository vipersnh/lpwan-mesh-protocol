#include <stdint.h>
#include <stdio.h>
#include <thread>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "interface.h"

bool g_is_master_node = false;
nodephy_t *g_node_phy = NULL;

using namespace std;

typedef enum {
    MAIN_THREAD_ID,
    BEACON_THREAD_ID,
    TIMER0_INSTANCE_ID,
    WAIT_FOR_BEACON_TX,
} thread_enum_t;

void beacon_thread_fn(nodethread_t *nodethread) {
    bool res;
    messenger_event_struct_t event_struct;
    radio_tx_packet_t tx_packet;
    radio_rx_packet_t rx_packet;
    if (!g_is_master_node && false) {
        radio_rx_enable_struct_t rx_enable_struct;
        memset(&rx_enable_struct, 0x00, sizeof(rx_enable_struct));
        rx_enable_struct.freq = RADIO_FREQ_433MHz;
        rx_enable_struct.channel = RADIO_CHANNEL_0;
        res = g_node_phy->rx_enable_from_thread(&rx_enable_struct, nodethread);
        assert(res);
        while (true) {
            printf("beacon_thread_fn[%d]: waiting now at %f\n", nodethread->get_node_id(), nodethread->get_time());
            nodethread->wait_for_event(WAIT_FOR_BEACON_TX, &event_struct);
            printf("beacon_thread_fn[%d]: resumed now at %f\n", nodethread->get_node_id(), nodethread->get_time());

            /* Rx has been enabled, wait for some duration for packet reception and check if packet was received */
            nodethread->sleep(30);
            do {
                res = g_node_phy->rx_packet_from_thread(&rx_packet, nodethread);
                printf("beacon_thread_fn[%d]: packet_receive output = %d at %f ", nodethread->get_node_id(), res, nodethread->get_time());
                if (res) {
                    printf(": ");
                    for (int i=0; i<10; i++) {
                        printf("0x%x, ", (unsigned char)rx_packet.packet[i]);
                    }
                } 
                printf("\n");
            } while (res);
        }
    }
    while (true) {
        /* We can transmit tx tx_packet */
        memset(&tx_packet, 0x00, sizeof(tx_packet));
        memset(&tx_packet.packet, 0xF1, sizeof(tx_packet.packet));
        tx_packet.packet_len = sizeof(tx_packet.packet);
        tx_packet.tx_rate = DATA_RATE_SF_08;
        tx_packet.tx_power = 0; /* dBm */
        tx_packet.freq = RADIO_FREQ_433MHz;
        tx_packet.channel = RADIO_CHANNEL_0;
        g_node_phy->tx_packet_from_thread(&tx_packet, nodethread);
        printf("beacon_thread_fn[%d]: packet sent at %f\n", nodethread->get_node_id(), nodethread->get_time());
        nodethread->sleep(60);
    }
}

void timer_callback(nodetimer_t *nodetimer) {
    printf("Called back from timer at %f\n", nodetimer->get_time());
}

int main(int argc, char *argv[]) {
    int node_id = -1;
    float delay = 0.3;
    int opt;
    float timer_period = 5;

    setbuf(stdout, NULL);

    while ((opt = getopt(argc, argv, "n:")) != -1) {
        switch (opt) {
        case 'n':
            node_id = atoi(optarg);
            break;
        default: /* '?' */
            exit(EXIT_FAILURE);
        }
    }

    if (node_id==-1) {
        printf("main : Error - invalid node-id passed\n");
        exit(EXIT_FAILURE);
    } else {
        printf("main : starting node_application with node-id %d\n", node_id);
    }

    if (node_id==0) { g_is_master_node = true; }

    g_node_phy = new nodephy_t(node_id);
    nodethread_t *main_thread = new nodethread_t(node_id, MAIN_THREAD_ID, NULL);
    main_thread->start();
    nodethread_t *thread1 = new nodethread_t(node_id, BEACON_THREAD_ID, &beacon_thread_fn);
    thread1->start();

    messenger_event_struct_t event_struct = {
        .event_id = WAIT_FOR_BEACON_TX,
        .buffer_len = 25,
        .buffer = {0,1,2,3},
    };    

    nodetimer_t * timer = new nodetimer_t(node_id, TIMER0_INSTANCE_ID, timer_period, timer_callback);
    timer->start_timer_from_thread(main_thread);

    bool timer_running = true;
    while (true) {
        main_thread->sleep(delay);
        printf("main_thread_fn : waken at %f\n", main_thread->get_time());
        main_thread->activate_event(&event_struct);
        if (main_thread->get_time() >= 20 && timer_running) {
            timer->stop_timer_from_thread(main_thread);
            timer_running = false;
        }
    }
    return 0;
}


