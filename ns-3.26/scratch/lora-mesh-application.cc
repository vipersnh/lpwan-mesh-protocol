#include "ns3/core-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("lora-mesh-application");

int main (int argc, char *argv[])
{
    NS_LOG_UNCOND ("lora-mesh-application : Running now");
    Simulator::Run ();
    Simulator::Destroy ();
}

