/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef LORA_MESH_H
#define LORA_MESH_H

namespace ns3 {

/* ... */

}

typedef struct {
    uint32_t id;
    uint32_t src_address;
    uint32_t dst_address;
} lora_message_t;

typedef void (*lora_ack_callback_t)(lora_message_t *);
typedef void (*lora_recv_callback_t(lora_message_t *);

class lora_mesh_node_t {
    public:
        lora_mesh_node_t ();
        void lora_send_message(lora_message_t *);
        bool lora_register_ack_callback(lora_ack_callback_t * callback);
        bool lora_register_recv_callback(lora_recv_callback_t * callback);

};

#endif /* LORA_MESH_H */

