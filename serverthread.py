from pdb import set_trace
import salabim as sim
from interface import recv_message, send_message
import interface_wrapper as intf

class serverthread_t(sim.Component):
    def __init__(self, sock, node_id, thread_id, servernode):
        super(serverthread_t, self).__init__()
        self.messenger_sock = sock
        self.node_id = node_id
        self.thread_id = thread_id
        self.cpp_node_thread = intf.serverthread_t(node_id, thread_id)
        self.servernode = servernode
        self.message_buffer = bytearray(intf.MAX_MESSAGE_SIZE)
        #print("serverthread->__init__ : Created with node_id {0}, thread_id {1} at {2}" .format(self.node_id, self.thread_id, self.env.now()))
        self.acknowledge_registration(True)

    def send_message(self):
        message_len = self.cpp_node_thread.copy_sending_message(self.message_buffer)
        send_message(self.messenger_sock, self.message_buffer, message_len)

    def acknowledge_registration(self, status):
        self.cpp_node_thread.send_rsp_status(intf.MESSENGER_THREAD_REGISTER_RSP, status)
        self.send_message()

    def get_recv_message_num(self):
        message = recv_message(self.messenger_sock)
        self.cpp_node_thread.copy_received_message(message)
        return self.cpp_node_thread.get_recv_message_num()

    def process(self):
        enable_enum_prints = False
        yield self.hold(self.servernode.get_server_time_offset())
        while True:
            msg_num = self.get_recv_message_num()
            if msg_num==intf.MESSENGER_REQ_SLEEP_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_SLEEP_CMD".format(self.node_id, self.thread_id))
                duration = self.cpp_node_thread.get_req_sleep_duration()
                yield self.hold(duration)
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_SLEEP_CMD, True)
            elif msg_num==intf.MESSENGER_REQ_GET_TIME_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_GET_TIME_CMD".format(self.node_id, self.thread_id))
                self.cpp_node_thread.send_rsp_current_time(self.env.now())
            elif msg_num==intf.MESSENGER_REQ_START_TIMER_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_START_TIMER_CMD".format(self.node_id, self.thread_id))
                timer_id = self.cpp_node_thread.get_req_timer_id()
                servertimer = self.servernode.get_node_timer_by_id(timer_id)
                servertimer.trigger_timer_start()
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_START_TIMER_CMD, True)
            elif msg_num==intf.MESSENGER_REQ_STOP_TIMER_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_STOP_TIMER_CMD".format(self.node_id, self.thread_id))
                timer_id = self.cpp_node_thread.get_req_timer_id()
                servertimer = self.servernode.get_node_timer_by_id(timer_id)
                servertimer.trigger_timer_stop()
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_STOP_TIMER_CMD, True)
            elif msg_num==intf.MESSENGER_REQ_TX_PACKET_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_TX_PACKET_CMD".format(self.node_id, self.thread_id))
                serverphy = self.servernode.get_server_phy()
                packet = bytearray(intf.MAX_MESSAGE_SIZE)
                cpp_packet = self.cpp_node_thread.get_tx_packet(packet)
                (tx_duration, status) = serverphy.handle_packet_transmission(cpp_packet)
                serverphy.activate(process="handle_packet_transmission_end", delay=tx_duration)
                # Ignore the transmit duration : yield self.hold(tx_duration)
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_TX_PACKET_CMD, status)
            elif msg_num==intf.MESSENGER_REQ_RX_EN_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_RX_EN_CMD".format(self.node_id, self.thread_id))
                rx_enable_struct = self.cpp_node_thread.get_rx_enable_struct()
                serverphy = self.servernode.get_server_phy()
                status = serverphy.enable_packet_reception(rx_enable_struct.freq, rx_enable_struct.channel)
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_RX_EN_CMD, True)
            elif msg_num==intf.MESSENGER_REQ_RX_DIS_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_RX_DIS_CMD".format(self.node_id, self.thread_id))
                serverphy = self.servernode.get_server_phy()
                status = serverphy.disable_packet_reception()
            elif msg_num==intf.MESSENGER_REQ_ACTIVATE_EVENT_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_ACTIVATE_EVENT_CMD".format(self.node_id, self.thread_id))
                event_struct = bytearray(intf.MAX_MESSAGE_SIZE)
                event_id = self.cpp_node_thread.get_event_id()
                cpp_event_struct = self.cpp_node_thread.get_messenger_event_struct(event_struct)
                self.servernode.set_event_wait_state(event_id, cpp_event_struct)
                self.cpp_node_thread.send_rsp_status(intf.MESSENGER_RSP_ACTIVATE_EVENT_CMD, True)
            elif msg_num==intf.MESSENGER_REQ_WAIT_FOR_EVENT_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_WAIT_FOR_EVENT_CMD".format(self.node_id, self.thread_id))
                event_id = self.cpp_node_thread.get_event_id()
                state = self.servernode.get_event_wait_state(event_id)
                yield self.wait(state)
                state.reset()
                cpp_event_struct = self.servernode.get_event_wait_struct(event_id)
                self.cpp_node_thread.send_messenger_event_struct(True, cpp_event_struct)
            elif msg_num==intf.MESSENGER_REQ_RX_PACKET_CMD:
                if enable_enum_prints:
                    print("serverthread[{0}][{1}] : Received MESSENGER_REQ_RX_PACKET_CMD".format(self.node_id, self.thread_id))
                serverphy = self.servernode.get_server_phy()
                recv_packet = serverphy.get_next_received_packet()
                if recv_packet==None:
                    self.cpp_node_thread.send_rx_packet_rsp(False, None)
                else:
                    self.cpp_node_thread.send_rx_packet_rsp(True, recv_packet.cpp_packet)
                    # Since cpp_packet has been copied, we must free it from the C context
                    intf.free_radio_rx_packet_copy(recv_packet.cpp_packet)
            else:
                print("Received msg_num = {0}".format(msg_num))
                assert 0
            self.send_message()
    
