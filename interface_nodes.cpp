#include <sys/socket.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <netinet/in.h>
#include <assert.h>
#include <arpa/inet.h>
#include <thread>
#include <netinet/tcp.h>
#include <stdint.h>
#include <mutex>

#include "interface.h"

using namespace std;

/* Implementation of nodethread_base_t */

/* ----> protected members */

void nodethread_base_t::send_message(message_enum_t msg_num, char *buffer, int buffer_len) {
    send_message_common(this->socket_fd, &this->mlock, msg_num, this->storage_buffer,
           sizeof(this->storage_buffer), buffer, buffer_len);
}

bool nodethread_base_t::recv_message(message_enum_t *msg_num, char * buffer, int buffer_len) {
    return recv_message_common(this->socket_fd, &this->mlock, msg_num, this->storage_buffer, 
            sizeof(this->storage_buffer), buffer, buffer_len);
}

/* ----> public members */

nodethread_base_t::nodethread_base_t(void) {
}

void nodethread_base_t::register_with_server(message_enum_t msg_num, message_enum_t expected_msg_num, int node_id, int instance_id) {

    {
        struct sockaddr_in serv_addr;
        if ((this->socket_fd = socket(AF_INET, SOCK_STREAM, 0))==0) {
            printf("nodethread_base->register_with_server socket failed\n");
            exit(EXIT_FAILURE);
        }

        memset(&serv_addr, 0x00, sizeof(serv_addr));

        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(SERVER_PORT);
          
        if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
            printf("nodethread_base[%d][%d]->register_with_server : Address not supported \n", node_id, instance_id);
            exit(EXIT_FAILURE);
        }    

        while (true) {
            if (connect(this->socket_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
                printf("nodethread_base[%d][%d]->register_with_server : Connection Failed retrying now\n", node_id, instance_id);
                usleep(1000);
            } else {
                break;
            }
        }

        int yes = 1;
        int result = setsockopt(this->socket_fd, IPPROTO_TCP, TCP_NODELAY, (char *) &yes, sizeof(int));
        assert (result==0);
    }

    messenger_register_struct_t register_struct = {
        .node_id = node_id,
        .instance_id = instance_id,
    };
    messenger_status_t status = { .result = false };

    this->send_message(msg_num, (char*)&register_struct, sizeof(register_struct));
    this->recv_message(&msg_num, (char*)&status, sizeof(status));

    assert (msg_num==expected_msg_num);
    assert (status.result);
}

void nodethread_base_t::sleep(float duration) {
    message_enum_t recv_msg;
    messenger_status_t status;
    messenger_sleep_cmd_struct_t sleep_cmd = { .duration = duration };
    this->send_message(MESSENGER_REQ_SLEEP_CMD, (char*)&sleep_cmd, sizeof(sleep_cmd));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_SLEEP_CMD);
    assert (status.result);
}

float nodethread_base_t::get_time(void) {
    message_enum_t recv_msg;
    float time;
    this->send_message(MESSENGER_REQ_GET_TIME_CMD, NULL, 0);
    (this->recv_message(&recv_msg, (char*)&time, sizeof(time)));
    assert (recv_msg==MESSENGER_RSP_GET_TIME_CMD);
    return time;
}


/* Implementation of nodethread_t */

nodethread_t::nodethread_t(int node_id, int thread_id, nodethread_fptr_t fptr) :
    nodethread_base_t() 
{
    this->node_id = node_id;
    this->instance_id = thread_id;

    if (fptr) {
        this->func_ptr = fptr;
    } else {
        this->func_ptr = NULL;
        this->is_mainthread = true;
    }

    /* Connect to the server and register as thread */
    this->register_with_server(MESSENGER_THREAD_REGISTER_CMD, MESSENGER_THREAD_REGISTER_RSP,
            node_id, thread_id);
    this->is_initialized = true;
}

int nodethread_t::get_node_id(void) {
    return this->node_id;
}

int nodethread_t::get_thread_id(void) {
    return this->instance_id;   
}

void nodethread_t::start(void) {
    if (this->is_mainthread) {
        this->thread_handle = NULL;
    } else {
        this->thread_handle = new thread(&nodethread_t::process, this);
    }
    this->should_stop = false;
}

void nodethread_t::process(void) {
    assert (this->func_ptr);
    while (this->should_stop==false) {
        (*this->func_ptr)(this);
    }
    delete this->thread_handle;
}

void nodethread_t::stop(void) {
    this->should_stop = true;
}

void nodethread_t::start_timer_from_thread(int timer_id) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_START_TIMER_CMD, (char*)&timer_id, sizeof(timer_id));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_START_TIMER_CMD);
    assert (status.result);
}

void nodethread_t::wait_for_event(int event_id, messenger_event_struct_t *event_struct) {
    messenger_status_t *status;
    char buffer[sizeof(messenger_status_t) + sizeof(messenger_event_struct_t)];
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_WAIT_FOR_EVENT_CMD, (char*)&event_id, sizeof(event_id));
    assert (this->recv_message(&recv_msg, (char*)buffer, sizeof(buffer)));
    assert (recv_msg==MESSENGER_RSP_WAIT_FOR_EVENT_CMD);
    status = (messenger_status_t*)buffer;
    assert (status->result);
    memcpy((char*)event_struct, buffer + sizeof(messenger_status_t), sizeof(messenger_event_struct_t));
}

void nodethread_t::activate_event(messenger_event_struct_t *event_struct) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_ACTIVATE_EVENT_CMD, (char*)event_struct, sizeof(messenger_event_struct_t));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_ACTIVATE_EVENT_CMD);
    assert (status.result);
}

void nodethread_t::stop_timer_from_thread(int timer_id) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_STOP_TIMER_CMD, (char*)&timer_id, sizeof(timer_id));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_STOP_TIMER_CMD);
    assert (status.result);
}

bool nodethread_t::tx_packet_from_thread(radio_tx_packet_t *packet) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_TX_PACKET_CMD, (char*)packet, sizeof(radio_tx_packet_t));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_TX_PACKET_CMD);
    assert (status.result);
    return status.result;
}

bool nodethread_t::rx_enable_from_thread(radio_rx_enable_struct_t *enable_struct) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_RX_EN_CMD, (char*)enable_struct, sizeof(radio_rx_enable_struct_t));
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_RX_EN_CMD);
    assert (status.result);
    return status.result;    
}

bool nodethread_t::rx_packet_from_thread(radio_rx_packet_t *rx_packet) {
    char buffer[sizeof(messenger_status_t) + sizeof(radio_rx_packet_t)];
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_RX_PACKET_CMD, NULL, 0);
    assert (this->recv_message(&recv_msg, buffer, sizeof(buffer)));
    memcpy((char*)&status, buffer, sizeof(status));
    assert (recv_msg==MESSENGER_RSP_RX_PACKET_CMD);
    if (status.result) {
        /* Extract RX-Packet into the rx_packet pointer */
        memcpy(rx_packet, buffer + sizeof(messenger_status_t), sizeof(radio_rx_packet_t));
    } else {
        memset(rx_packet, 0x00, sizeof(radio_rx_packet_t));
    }
    return status.result;    
}

bool nodethread_t::rx_disable_from_thread(void) {
    messenger_status_t status;
    message_enum_t recv_msg;
    this->send_message(MESSENGER_REQ_RX_DIS_CMD, NULL, 0);
    assert (this->recv_message(&recv_msg, (char*)&status, sizeof(status)));
    assert (recv_msg==MESSENGER_RSP_RX_DIS_CMD);
    assert (status.result);
    return status.result;    
}

/* Implementation of nodetimer_t */

nodetimer_t::nodetimer_t(int node_id, int timer_id, float period, nodetimer_callback_fptr_t fptr) :
    nodethread_base_t() 
{
    this->node_id = node_id;
    this->instance_id = timer_id;
    this->period = period;
    this->offset_enabled = false;
    this->offset = 0;
    this->callback_handler = fptr;

    this->register_with_server(MESSENGER_TIMER_REGISTER_CMD, MESSENGER_TIMER_REGISTER_RSP,
            node_id, timer_id);
    this->is_initialized = true;

    this->thread_handle = new thread(&nodetimer_t::process, this);
    this->should_stop = false;
}

int nodetimer_t::get_timer_id(void) {
    return this->instance_id;
}

void nodetimer_t::add_offset(float delay) {
    this->offset = delay;
    this->offset_enabled = true;
}

void nodetimer_t::start_timer_from_thread(nodethread_t *thread) {
    int timer_id = this->get_timer_id();
    thread->start_timer_from_thread(timer_id);
}

void nodetimer_t::stop_timer_from_thread(nodethread_t *thread) {
    int timer_id = this->get_timer_id();
    thread->stop_timer_from_thread(timer_id);
}

void nodetimer_t::stop_timer_from_timer_callback(void) {
    assert (0);
}
void nodetimer_t::update_callback_function(nodetimer_callback_fptr_t fptr) {
    assert (fptr!=NULL);
    this->callback_handler = fptr;
}

void nodetimer_t::process(void) {
    while (this->should_stop==false) {
        if (this->offset_enabled) {
            this->sleep(this->offset);
            this->offset_enabled = false;
        }
        this->sleep(this->period);
        assert (this->callback_handler);
        (*this->callback_handler)(this);
    }
    delete this->thread_handle;
}

/* Implementation of nodephy_t */

int nodephy_t::num_instances = 0;

nodephy_t::nodephy_t(int node_id) {
    nodephy_t::num_instances += 1;
    this->node_id = node_id;
    this->instance_id = 0;
    assert (nodephy_t::num_instances==1);

    this->register_with_server(MESSENGER_PHY_REGISTER_CMD, MESSENGER_PHY_REGISTER_RSP,
           this->node_id, this->instance_id);
    this->is_receiving = false;
    this->is_initialized = true;
}

bool nodephy_t::tx_packet_from_thread(radio_tx_packet_t *packet, nodethread_t *thread) {
    bool res = false;
    this->mlock.lock();
    res = thread->tx_packet_from_thread(packet);
    this->mlock.unlock();
    return res;
}

bool nodephy_t::rx_enable_from_thread(radio_rx_enable_struct_t *enable_struct, nodethread_t *thread) {
    bool res = false;
    this->mlock.lock();
    if (this->is_receiving) {
        res = false;
    } else {
        res = thread->rx_enable_from_thread(enable_struct);
        this->is_receiving = res ? true : false;
    }
    this->mlock.unlock();
    return res;
}

bool nodephy_t::rx_packet_from_thread(radio_rx_packet_t *rx_packet, nodethread_t *thread) {
    bool res = false;
    /* Do not do the below assert since we are receiving from a packet queue of received packets */
    /* assert (this->is_receiving); */
    this->mlock.lock();
    res = thread->rx_packet_from_thread(rx_packet);
    this->mlock.unlock();
    return res;
}

bool nodephy_t::rx_disable_from_thread(nodethread_t *thread) {
    bool res = false;
    this->mlock.lock();
    if (this->is_receiving) {
        res = thread->rx_disable_from_thread();
        this->is_receiving = res ? false : true;
    } else {
        res = false;
    }
    this->mlock.unlock();
    return res;
}
